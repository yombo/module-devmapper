Yombo Device Mapper Module
===========================

Simply maps one device to another. OR...Bridges one protocol to anther.  This
is the primary use case of Yombo Gateway.  This module allows Yombo to act as
a bridge between different vendor protocols.

Examples:
Create an X10 device and use it to control an Insteon or Z-wave device.
