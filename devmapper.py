"""
Simply maps one device to another. OR...Bridges one protocol to anther.  This
is the primary use case of Yombo Gateway.  This module allows Yombo to act as
a bridge between different vendor protocols.

Examples:
Create an X10 device and use it to control an Insteon or Z-wave device.

.. moduleauthor:: Mitch Schwenk <mitch-gw@yombo.net>
:copyright: Copyright 2012 by Yombo.
:license: See LICENSE file for details.
"""
from yombo.core.module import YomboModule
from yombo.core.helpers import getTimes
from yombo.core.log import getLogger

logger = getLogger("module.devmapper")

class DevMapper(YomboModule):
    """
    Handles the work of protocol bridging.
    """
    def init(self):
        """
        Just register to receive cmd and status messages.
        """
        self._ModDescription = "Device mapper module to act as a protocol bridge."
        self._ModAuthor = "Mitch Schwenk @ Yombo"
        self._ModUrl = "http://www.yombo.net"
        self._RegisterDistributions = ['cmd', 'status']

    def load(self):
        """
        Go through each device seeing if a device variable is found called
        "devMapperDevice" to know if we should be briding this device.
        
        Logic:
        for each device:
         if our device variable is set and is has a valid deviceObj
           add both devices to our list
        """
        self.ourDevices = {}
        for device in self._Devices:
          if 'devMapperDevice' in device.deviceVariables:
            newDevice = None
            try:
              newDevice = self._Devices[device.deviceVariables['devMapperDevice']]
            except:
              continue;
            # Have a valid device and we do care about it. Now, add a mapping
            # for each way..
            self.ourDevices[newDevice.deviceUUID] = device
            self.ourDevices[device.deviceUUID] = newDevice
        
    def start(self):
        pass
    
    def stop(self):
        pass
    
    def unload(self):
        pass

    def message(self, message):
        """
        Listen to all traffic.  For each deviceobj, check to see if we care
        about it. If we do, then duplicate the command to it's counter part.
        """
        if 'deviceobj' in message.payload:
          if message.payload['deviceobj'].deviceUUID in self.ourDevices:
            if message.msgType == "cmd":
              destDevice = self.ourDevices['message.payload['deviceobj'].deviceUUID']

              msgItems = {cmdobj = message.payload['cmdobj'],
                          payload = message.payload['payload'],
                         }
              if 'pinnumber' in message.payload:
                  msgItems['pinnumber'] = message.payload['pinnumber']
              if 'skippinnumber' in message.payload:
                  msgItems['skippinnumber'] = message.payload['skippinnumber']
                  
              newMessage = getMessage(self, **msgItems)
              newMessage.send()
              
            elif message.msgType == "status":
              destDevice = self.ourDevices['message.payload['deviceobj'].deviceUUID']
              destDevice.setStatus( status = message.payload['status']['status'],
                                    statusExtra = message.payload['status']['statusextra'],
                                    source = message.msgOrigin,
                                    payload = message.payload
                                  )
